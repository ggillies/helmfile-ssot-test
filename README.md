# Helmfile SSOT Test/POC

This is for https://gitlab.com/gitlab-com/gl-infra/delivery/issues/597#note_276609144

The requirements for using this repo are as follows

* helm 2 installed
* helm diff plugin installed https://github.com/databus23/helm-diff
* helm tillerless plugin installed https://github.com/rimusz/helm-tiller
* helm git plugin installed https://github.com/aslafy-z/helm-git **Make sure to install version 0.5.0 as installing from master is broken**
* helm s3 plugin installed https://github.com/hypnoglow/helm-s3
* helmfile installed https://github.com/roboll/helmfile
* Google Cloud SDK installed and configured with your gitlab credentials
* You will need to generate a personal API key on ops.gitlab.net (with read repository permissions), then export an environment variable containing it
```
export GITLAB_API_TOKEN=asdf1234
```

You should be able to test the output of this work with
```
helmfile -e gstg diff --suppress-secrets
```

If you are adventurous you can apply it!
```
helmfile -e gstg apply
```

If you wish to only test a single helm chart you can name it specifically like the following
```
helmfile -l name=gitlab -e gstg diff --suppress-secrets
```
